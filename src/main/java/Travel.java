import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.*;
import java.util.ArrayList;

/**
 * Travel Class
 * @author Marc Szymkowiak
 * @version 1.0
 */
public class Travel {
    private int id;
    private String description;
    private ArrayList<Place> places;
    private ArrayList<Direction> directions;

    /**
     * Constructor
     */
    public Travel() {
        this.places = new ArrayList<Place>();
        this.directions = new ArrayList<Direction>();
    }

    /**
     * set the travel id
     * @param id the travel id
     */
    private void setId(int id) {
        this.id = id;
    }

    /**
     * get the travel id
     * @return the travel id
     */
    public int getId() {
        return this.id;
    }

    /**
     * set the travel description
     * @param description the travel description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * get the travel description
     * @return
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * add a place to the travel
     * @param place the place object
     */
    public void addPlace(Place place) {
        this.places.add(place);
    }

    /**
     * return all places
     * @return list of place objects
     */
    public ArrayList<Place> getPlaces() {
        return places;
    }

    /**
     * add a direction between two places
     * @param direction the direction object
     */
    public void addDirection(Direction direction) {
        this.directions.add(direction);
    }

    /**
     * return all directions
     * @return list of direction objects
     */
    public ArrayList<Direction> getDirections() {
        return directions;
    }

    /**
     * serialize class as xml
     */
    public void serialize(String filename) throws IOException {
        ObjectMapper xmlMapper = new XmlMapper();
        xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        xmlMapper.writeValue(new File(filename), this);
    }

    /**
     * deserialize xml to travel file
     */
    public void deserialize(String filePath) throws IOException {
        File file = new File(filePath);
        XmlMapper xmlMapper = new XmlMapper();
        String xml = inputStreamToString(new FileInputStream(file));
        Travel tmp = xmlMapper.readValue(xml, Travel.class);
        this.id = tmp.id;
        this.places = tmp.places;
        this.directions = tmp.directions;
        this.description = tmp.description;
    }

    /**
     * Helper class for deserialize xml. Read all lines from a text file into a string
     * @param is the inputstream
     * @return value of xml as string
     * @throws IOException
     */
    private String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }

}
