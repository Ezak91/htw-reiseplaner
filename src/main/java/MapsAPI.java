import com.google.maps.*;
import com.google.maps.errors.ApiException;
import com.google.maps.model.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import static org.jsoup.Jsoup.clean;

/**
 * API class
 * @author Marc Szymkowiak
 * @version 1.0
 */
public class MapsAPI {
    private static String API_KEY = "AIzaSyCz1DKCQV9ixMomLQ7wUaM8i1rM7qCyvR0";
    private static String LANGUAGE = "de";
    private GeoApiContext context;

    /**
     * Constructor
     */
    public MapsAPI() {
        context = new GeoApiContext();
        context.setApiKey(API_KEY);
    }

    /**
     * search places for a given query
     * @param query the query to search for
     * @return a list of found places for query
     */
    public ArrayList<Place> searchPlaces(String query) throws ApiException, InterruptedException, IOException{
        Place place;
        ArrayList<Place> places = new ArrayList<Place>();
        GeocodingResult[] results = GeocodingApi.geocode(context, query).await();
        for(GeocodingResult result : results ) {
            place = new Place();
            place.setId(result.placeId);
            place = getDetailsForPlace(place);
            places.add(place);
        }
        return places;
    }

    /**
     * search places near to anoter place
     * @param startPlace the place to start the search from
     * @param rad the radius for searching a place
     * @param type the placetype to search for
     * @return a list of places in near of another place
     */
    public ArrayList<Place> searchNearPlaces(Place startPlace, int rad, PlaceType type) throws ApiException, InterruptedException, IOException {
        Place place;
        ArrayList<Place> places = new ArrayList<Place>();
        LatLng latlng = new LatLng(startPlace.getLat(), startPlace.getLng());
        NearbySearchRequest request = PlacesApi.nearbySearchQuery(context, latlng);
        request.radius(rad);
        request.type(type);

        PlacesSearchResponse response = request.await();
        PlacesSearchResult[] results = response.results;

        for(PlacesSearchResult result : results) {
            place = new Place();
            place.setId(result.placeId);
            place = getDetailsForPlace(place);
            places.add(place);
        }

        return places;
    }

    /**
     * get detail for a place
     * @param place the place to load the details for
     * @return place object with details
     * @throws ApiException
     * @throws InterruptedException
     * @throws IOException
     */
    public Place getDetailsForPlace(Place place) throws ApiException, InterruptedException, IOException {
        PlaceDetailsRequest request = PlacesApi.placeDetails(context,place.getId());
        request.language(LANGUAGE);
        PlaceDetails details = request.await();

        place.setDescription(details.name);
        place.setRating(details.rating);
        place.setLat(details.geometry.location.lat);
        place.setLng(details.geometry.location.lng);
        place.setFormattedAddress(details.formattedAddress);

        for(AddressComponent component : details.addressComponents ) {
            if(componentHasType(AddressComponentType.STREET_NUMBER,component.types)) {
                place.setNumber(component.longName);
                continue;
            }
            if(componentHasType(AddressComponentType.POSTAL_CODE, component.types)) {
                place.setPlz(Integer.parseInt(component.longName));
                continue;
            }
            if(componentHasType(AddressComponentType.ROUTE, component.types)) {
                place.setStreet(component.longName);
                continue;
            }
            if(componentHasType(AddressComponentType.LOCALITY,component.types)) {
                place.setPlaceName(component.longName);
                continue;
            }
        }

        return place;
    }

    /**
     * get a direction for two places
     * @param origin the origin place
     * @param destination the destination place
     * @param travelMode the travel mode
     * @return direction object
     */
    public Direction getDirection(Place origin, Place destination, TravelMode travelMode) throws  Exception{

        Direction direction = new Direction(origin,destination,travelMode);
        StringBuffer directionTextBuffer = new StringBuffer();
        Image image;

        DirectionsApiRequest request = DirectionsApi.getDirections(context, origin.getFormattedAddress(),
                destination.getFormattedAddress());
        request.language(LANGUAGE);
        request.mode(travelMode);
        DirectionsResult result = request.await();

        image = getDirectionImage(result.routes[0].overviewPolyline.getEncodedPath());

        DirectionsStep[] steps = result.routes[0].legs[0].steps;
        for(DirectionsStep step : steps) {
            directionTextBuffer.append(clean(step.htmlInstructions, Whitelist.none()));
            directionTextBuffer.append("\n");
        }

        direction.setDirectionText(directionTextBuffer.toString());
        direction.setImage(image);

        return direction;
    }

    /**
     * Get Image for Direction
     * @param encRoute encoded Route
     * @return Image
     * @throws Exception
     */
    public Image getDirectionImage(String encRoute) throws Exception {
        String apiUrl = "https://maps.googleapis.com/maps/api/staticmap?size=%SIZE%&scale=2&path=enc%3A%PATH%&key=%KEY%";
        apiUrl = apiUrl.replace("%SIZE%", "1920x1080");
        apiUrl = apiUrl.replace("%PATH%", encRoute);
        apiUrl = apiUrl.replace("%KEY%", API_KEY);
        URL url = new URL(apiUrl);
        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
        InputStream inputStream = httpConn.getInputStream();
        Image image = ImageIO.read(inputStream);
        return image;
    }

    /**
     * compare a given componenttype with a list of componenttypes
     * @param type the type to validate
     * @param types list of types
     * @return true if list type is in types otherwise false
     */
    private boolean componentHasType(AddressComponentType type, AddressComponentType[] types) {
        for(AddressComponentType comtype : types) {
            if(comtype.toString() == type.toString()) {
                return true;
            }
        }
        return false;
    }

}
