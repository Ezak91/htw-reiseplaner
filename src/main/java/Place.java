/**
 * Place class
 * @author Marc Szymkowiak
 * @version 1.0
 */
public class Place {
    private String id;
    private String description;
    private double rating;
    private String street;
    private String number;
    private int plz;
    private double lat;
    private double lng;
    private int sortNumber;
    private String placeName;
    private String formattedAddress;

    /**
     * Constructor
     */
    public Place() {

    }

    /**
     * get the place id
     * @return the place id
     */
    public String getId() {
        return id;
    }

    /**
     * set the place id
     * @param id the place id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * get the place description
     * @return the place description
     */
    public String getDescription() {
        return description;
    }

    /**
     * set the place description
     * @param description the place description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * get the place rating
     * @return the place rating
     */
    public double getRating() {
        return rating;
    }

    /**
     * set the place rating
     * @param rating the place rating
     */
    public void setRating(double rating) {
        this.rating = rating;
    }

    /**
     * get the place street
     * @return the place street
     */
    public String getStreet() {
        return street;
    }

    /**
     * set the place street
     * @param street the place street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * get the place numbert
     * @return the place number
     */
    public String getNumber() {
        return number;
    }

    /**
     * set the place number
     * @param number the place numbert
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * get the place plz
     * @return the place plz
     */
    public int getPlz() {
        return plz;
    }

    /**
     * set the place plz
     * @param plz the place plz
     */
    public void setPlz(int plz) {
        this.plz = plz;
    }

    /**
     * get the geolocation lat
     * @return the geolocation lat
     */
    public double getLat() {
        return lat;
    }

    /**
     * set the geolocation lat
     * @param lat
     */
    public void setLat(double lat) {
        this.lat = lat;
    }

    /**
     * get the geolocation lng
     * @return the geolocation lng
     */
    public double getLng() {
        return lng;
    }

    /**
     * set the geolocation lng
     * @param lng
     */
    public void setLng(double lng) {
        this.lng = lng;
    }

    /**
     * get the sortnumber of the place
     * @return the sortnumber
     */
    public int getSortNumber() {
        return sortNumber;
    }

    /**
     * set the sortnumber of the place
     * @param sortNumber
     */
    public void setSortNumber(int sortNumber) {
        this.sortNumber = sortNumber;
    }

    /**
     * get the place name
     * @return the place name
     */
    public String getPlaceName() {
        return placeName;
    }

    /**
     * set the place name
     * @param placeName the place name
     */
    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    /**
     * get the formatted address
     * @return the formatted address
     */
    public String getFormattedAddress() {
        return formattedAddress;
    }

    /**
     * set the formatted address
     * @param formattedAddress the formatted address
     */
    public void setFormattedAddress(String formattedAddress) {
        this.formattedAddress = formattedAddress;
    }
}