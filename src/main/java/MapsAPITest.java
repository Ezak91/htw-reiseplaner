import com.google.maps.model.*;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Marc Szymkowiak on 18.06.17.
 */
public class MapsAPITest {
    private static MapsAPI api;

    public static void main(String [] args)
    {
        api = new MapsAPI();
        ArrayList<Place> places;
        Direction dir;
        try {
            places = api.searchPlaces("Bahnhof Saarbrücken");
            places = api.searchNearPlaces(places.get(0),20000,PlaceType.MOVIE_THEATER);
            dir = api.getDirection(places.get(0), places.get(1), TravelMode.WALKING);
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
        
    }

}
