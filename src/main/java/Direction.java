import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.maps.model.TravelMode;
import  java.awt.*;

/**
 * Place class
 * @author Marc Szymkowiak
 * @version 1.0
 */
@JsonIgnoreProperties(value = { "image" })
public class Direction {
    private TravelMode mode;
    private Place origin;
    private Place destination;
    private String directionText;
    private Image image;

    /**
     * Constructor
     * @param origin the start place
     * @param destination the destination place
     * @param mode the travel mode
     */
    @JsonCreator
    public Direction(@JsonProperty("origin") Place origin, @JsonProperty("destination") Place destination,  @JsonProperty("mode") TravelMode mode) {
        this.origin = origin;
        this.destination = destination;
        this.mode = mode;
    }

    /**
     * get the direction text
     * @return the direction text
     */
    public String getDirectionText() {
        return this.directionText;
    }

    /**
     * gets the image for the direction
     * @return the direction image
     */
    public Image getImage() {
        return image;
    }

    /**
     * set the image for the direction
     * @param image
     */
    public void setImage(Image image) {
        this.image = image;
    }

    /**
     * set the direction text
     * @param directionText the direction text
     */
    public void setDirectionText(String directionText) {
        this.directionText = directionText;
    }

    /**
     * get the travelmode
     * @return the travelmode
     */
    public TravelMode getMode() {
        return mode;
    }

    /**
     * get the origin place
     * @return the origin place
     */
    public Place getOrigin() {
        return origin;
    }

    /**
     * get the destination place
     * @return the destination place
     */
    public Place getDestination() {
        return destination;
    }
}
